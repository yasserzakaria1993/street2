<?php
/**
 * The main template file
 *
* Template Name: Apply
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Move_Street
 */

get_header();
?>
  <?php if(have_rows('layout')): ?>
    <?php while(have_rows('layout')): ?>
      <?php the_row(); ?>
  <div class="apply-page-wrapper">
        <?php if(get_row_layout()=='hero_section'): ?>
    <section class="main-section">
      <img src="<?php the_sub_field('background_image'); ?>" alt="" class="bg">
      <div class="container">
        <div class="row justify-content-center align-items-center">
          <div class="col-12 col-lg-8">
            <h2 class="mos-title-1 iv-wp-from-bottom"><?php the_sub_field('big_title'); ?></h2>
            <p class="mos-paragraph iv-wp-from-bottom"><?php the_sub_field('description'); ?></p>
          </div>
          <div class="col-12 col-lg-4  iv-wp-from-bottom">
            <?php $button=get_sub_field('button'); ?>
          <a href="<?php echo $button['url']; ?>"><button class="main-btn yellow hover-arrow"><?php echo $button['title']; ?> <i class="fal fa-long-arrow-right"></i></button></a>
          </div>
        </div>
      </div>
    </section>
  <?php elseif(get_row_layout()=='why_choose_us_section'): ?>
    <section class="why-work-with-us-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12">
            <h3 class="title iv-wp-from-bottom"><?php the_sub_field('title'); ?></h3>
            <h2 class="subtitle iv-wp-from-bottom"><?php the_sub_field('description'); ?></h2>
          </div>
          <?php if(have_rows('reasons')):
            while(have_rows('reasons')):
            the_row();?>
          <div class="col-12 col-md-6 col-lg-3">
            <div class="reason">
              <img class="iv-wp-from-top" src="<?php the_sub_field('reason_image'); ?>" alt="">
              <div class="description iv-wp-from-bottom"><?php the_sub_field('reason_text'); ?>
              </div>
            </div>
          </div>
        <?php endwhile; endif; ?>

        </div>
      </div>
    </section>
  <?php elseif(get_row_layout()=='focus_on_what_you_do_best'): ?>
    <section class="focus-section">
      <div class="container">
        <div class="filter"></div>
        <img src="<?php the_sub_field('image_below_button'); ?>" alt="" class="bg iv-wp-from-bottom">
        <div class="row justify-content-center">
          <div class="col-12">
            <h3 class="title iv-wp-from-bottom"><?php the_sub_field('section_title'); ?></h3>
            <h2 class="subtitle iv-wp-from-bottom"><?php the_sub_field('section_description'); ?></h2>
          </div>
          <?php if(have_rows('points')):
            while(have_rows('points')):
              the_row(); ?>
          <div class="col-12 col-md-6">
            <div class="reason">
              <img class="iv-wp-from-top" src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/apply-tick.png" alt="">
              <div class="description iv-wp-from-bottom"><?php the_sub_field('title'); ?></div>
            </div>
          </div>
        <?php endwhile; endif; ?>
          <div class="col-12 iv-wp-from-bottom" style="z-index:999">
            <?php $button=get_sub_field('button'); ?>
          <a href="<?php echo $button['url']; ?>">  <button class="sub-btn hover-arrow"><?php echo $button['title']; ?> <i class="fal fa-long-arrow-right"></i></button></a>
          </div>
        </div>
      </div>
      <div class="aspect-ratio">
        <svg preserveAspectRatio="none" viewBox="0 0 1 1">
          <defs>
            <clipPath clipPathUnits="objectBoundingBox" id="hero-clip-path">
              <path d="M0 0 H1 V1 L.14 .528 L0 .73 Z"/>
            </clipPath>
          </defs>
          <path d="M0 0H1V.834L.862 1L0 .6Z" fill="#f6fafe"/>
        </svg>
      </div>
    </section>
  <?php elseif (get_row_layout()=='requirements'): ?>
    <section class="requirements">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="title iv-wp-from-top">Our requirements</div>
          </div>
          <div class="col-12 col-lg-6">
            <img class="man iv-wp-from-bottom" src="<?php the_sub_field('left_image'); ?>" alt="">
          </div>
          <div class="col-12 col-lg-6">
            <div class="requirements-container">
              <?php if(have_rows('requirements')):
                while (have_rows('requirements')):
                  the_row(); ?>
              <div class="requirement">
                <img class="iv-wp-from-bottom" src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/apply-tick.png" alt="">
                <div class="description iv-wp-from-bottom"><?php the_sub_field('requirement'); ?></div>
              </div>
            <?php endwhile; endif; ?>
          </div>
        </div>
      </div>
    </section>
  <?php elseif(get_row_layout()=='faq'): ?>
    <section class="faqs">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="title iv-wp-from-bottom">
            <?php the_sub_field('section_title'); ?>
              <span class="primary-color">.</span>
            </h2>
          </div>

          <div class="faqs-container col-12">
            <?php if(have_rows('questions')): ?>
            <?php while(have_rows('questions')):
              the_row(); ?>
            <div class="question-block iv-wp-from-bottom">
              <div class="question"><?php the_sub_field('question_title'); ?>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/apply-plus.png" alt="">
              </div>
              <div class="answer">
                <div class="answer-wrapper"><?php the_sub_field('answer'); ?>
                </div>
              </div>
            </div>
          <?php endwhile; endif; ?>
          </div>
        </div>
      </div>
    </section>
<?php endif; ?>
  </div>
<?php endwhile; endif; ?>

	<?php
get_footer();
