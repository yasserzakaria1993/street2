<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Move_Street
 */

?>
<?php if (have_rows('top_footer_section', 'option')){
while (have_rows('top_footer_section', 'option')){
the_row(); ?>
<footer>
	<?php if (is_page('apply')): ?>
    <div class="upper-part">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/footer-bg.png" alt="" class="footer-bg">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/footer-truck.png" alt="" class="iv-wp-from-left-car car">
      <div class="container">
        <h2 class="title iv-wp-from-top">
					<?php the_sub_field('big_slogan'); ?>
        </h2>
        <div class="iv-wp-from-bottom">
					<?php $button = get_sub_field('apply_button'); ?>
          <a href="<?php echo $button['url']; ?>">
            <button class="main-btn yellow hover-arrow"><?php echo $button['title']; ?> <i class="fal fa-long-arrow-right"></i></button>
          </a>
        </div>
      </div>
    </div>
	<?php elseif (is_front_page()): ?>
    <section class="ready-to-book-your-move">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-11 col-sm-10">
            <h2 class="mos-title-2 text-center iv-wp-from-bottom">
              Ready to book your move?
            </h2>
            <h5 class="mos-paragraph text-center iv-wp-from-bottom">
              Schedule an appointment today.
            </h5>
          </div>
          <div class="footer-form iv-wp-from-bottom">
            <div class="container">
              <div class="row">
								<?php
								$form_id = '4';
								gravity_form($form_id, false, false, false, '', true, 12);
								?>
              </div>
              <!--              <div class="row">-->
              <!--                -->
              <!--                <div class="col-md-6">-->
              <!--                  <input placeholder="Pick-up Postcode" type="text">-->
              <!--                </div>-->
              <!--                <div class="col-md-6">-->
              <!--                  <select>-->
              <!--                    <option value="1">Pick-up Property Type</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                  </select>-->
              <!--                </div>-->
              <!--                <div class="col-md-6">-->
              <!--                  <select>-->
              <!--                    <option value="1">Property Size</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                    <option value="1">asdas</option>-->
              <!--                  </select>-->
              <!--                </div>-->
              <!--                <div class="col-md-6">-->
              <!--                  <input placeholder="Drop-off Postcode" type="text">-->
              <!--                </div>-->
              <!--                <div class="col-md-6">-->
              <!--                  <input placeholder="Email" type="text">-->
              <!--                </div>-->
              <!--                <div class="col-md-6">-->
              <!--                  <input placeholder="Mobile" type="text">-->
              <!--                </div>-->
              <!--                <div class="col-md-6 view-price-btn-wrapper mx-auto">-->
              <!--                  <button class="main-btn" type="submit">VIEW PRICE</button>-->
              <!--                </div>-->
              <!--              </div>-->
            </div>
          </div>
        </div>
      </div>
      <div class="building-img iv-wp-from-bottom">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/home/footer-building.png" alt="" class="">
      </div>
    </section>
    <div class="upper-part-home">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/apply/footer-truck.png" alt="" class="home-footer-car iv-wp-from-left-car">
    </div>
	<?php endif; ?>
  <div class="lower-part" <?php if (is_page('service')) {
		echo 'style="padding-top:92px"';
	} ?>>
    
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h3 class="call-us iv-wp-from-bottom">
            Questions? Give us a call! <span><?php the_sub_field('phone_number'); ?></span>
          </h3>
        </div>
      </div>
			<?php }
			} ?>
      <hr>
			<?php if (have_rows('bottom_footer_section', 'option')){
			while (have_rows('bottom_footer_section', 'option')){
			the_row();
			?>
      <div class="row justify-content-around">
        <div class="col-12 col-md-6 col-xl-3 mr-xl-auto text-center text-xl-left">
          <img src="<?php the_sub_field('footer_logo'); ?>" alt="" class="logo iv-wp-from-left">
					<?php if (get_sub_field('book_button')) {
						$button = get_sub_field('book_button');
						?>
            <a href="<?php echo $button['url']; ?>">
              <button class="book-now iv-wp-from-left"><?php echo $button['title']; ?></button>
            </a>
					<?php } ?>
          <div class="have-account iv-wp-from-bottom">
            Already have an account? <br>
            <a href="#"><span class="hover-arrow">Login <i class="fal fa-long-arrow-right"></i></span></a>
          </div>
        </div>
        <div class="col-12 d-xl-none"></div>
        <!-- footer Links -->
				<?php if (have_rows('footer_links')) {
					while (have_rows('footer_links')) {
						the_row(); ?>
						<?php if (have_rows('links_list')) {
							while (have_rows('links_list')) {
								the_row(); ?>
                <div class="col-12 col-sm-4 col-xl-auto">
                  <div class="footer-links">
                    <a class="iv-wp-from-bottom"><?php the_sub_field('list_title'); ?></a>
										<?php if (have_rows('list_item')) {
											while (have_rows('list_item')) {
												the_row();
												$link = get_sub_field('item');
												?>
                        <a class="iv-wp-from-bottom" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
											<?php }
										} ?>
                  </div>
                </div>
							<?php }
						} ?>
					<?php }
				} ?>
				<?php }
				} ?>
      </div>
			
			<?php if (have_rows('copyright_section', 'option')){
			while (have_rows('copyright_section', 'option')){
			the_row();
			?>
      <div class="row">
				<?php if (have_rows('social_links')) {
					while (have_rows('social_links')) {
						the_row(); ?>
            <div class="col-12">
              <div class="social-links iv-wp-from-bottom-footer">
                <a href="<?php the_sub_field('youtube'); ?>" class="social-link "><i class="fab fa-youtube"></i></a>
                <a href="<?php the_sub_field('twitter'); ?>" class="social-link "><i class="fab fa-twitter"></i></a>
                <a href="<?php the_sub_field('facebook'); ?>" class="social-link "><i class="fab fa-facebook-f"></i></a>
              </div>
            </div>
					<?php }
				} ?>
        <div class="col-12">
          <div class="copy-rights">
						<?php the_sub_field('copyright_text'); ?>
						<?php $link_1 = get_sub_field('link_1'); ?>
						<?php $link_2 = get_sub_field('link_2'); ?>
            <div class="footer-last-links">
              <a href="<?php echo $link_1['url']; ?>"><span><?php echo $link_1['title']; ?></span></a>
              <span>|</span>
              <a href="<?php echo $link_2['url']; ?>"><span><?php echo $link_2['title']; ?></span></a>
            </div>
          </div>
        </div>
				<?php }
				} ?>
      </div>
    
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
<script>
  // $(document).ready(function () {
  //   $('.footer-form').find("#input_1_8").attr('placeholder', 'Pick-up Postcode');
  //   $('.footer-form').find("#input_1_4").attr('placeholder', 'Property Size');
  //   $('.footer-form').find("#input_1_5").attr('placeholder', 'Drop-off Postcode');
  //   $('.footer-form').find("#input_1_9").attr('placeholder', 'Email');
  //   $('.footer-form').find("#input_1_7").attr('placeholder', 'Mobile');
  //   $('#a7a').load(function () {
  //     $('#a7a').contents().find('body');
  //   });
  // $('iframe').contents().find('#u_0_2').css('color', '#fff');
  // console.log('asd');
  
  // });
  // $().ready(function () {
  //   $("#a7a").ready(function () { //The function below executes once the iframe has finished loading
  //     $('#u_0_2', frames['a7aa'].document).css('color', '#fff');
  //   });
  // };


</script>
</body>
</html>
