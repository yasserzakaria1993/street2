import 'slick-carousel';

export const home = () => {
  const $instagram = $('section.instagram-photos').slick({
    infinite: true,
    slidesToShow: 15,
    slidesToScroll: 1,
    touchThreshold: 30,
    arrows: false,
    dots: false,
    centerMode: true,
    centerPadding: '60px',
    draggable: true,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 2000.98,
        settings: {
          slidesToShow: 9,
        },
      }, {
        breakpoint: 1600.98,
        settings: {
          slidesToShow: 7,
        },
      }, {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 5,
        },
      }, {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 3,
        },
      },
    ],
  });
  const $testimonials = $('.testimonial-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    touchThreshold: 30,
    arrows: false,
    dots: false,
    centerMode: true,
    centerPadding: '0px',
    draggable: true,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 767.98,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '100px',
        },
      },
      {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          centerPadding: '50px',
        },
      },
    ],
  });
  const $clients = $('.clients-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    touchThreshold: 30,
    arrows: false,
    dots: false,
    centerMode: true,
    centerPadding: '0px',
    draggable: true,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 991.98,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 575.98,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
  
  
  
  $('.navbar-collapse-wrapper').on('click',function () {
    $('.nav-bar-right-mobile').addClass('active');
  });
  $('.mobile-menu-close').on('click',function () {
    $('.nav-bar-right-mobile').removeClass('active');
  });
};