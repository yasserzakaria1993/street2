<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Move_Street
 */

?>
<!doctype html>
<!--suppress CheckEmptyScriptTag -->
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title><?php echo bloginfo('name'); ?></title>
  <?php wp_head(); ?>
<body class="home page-<?php echo the_ID(); ?>">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=327645063982830"></script>
    <div id="loading">
        <div class="lds-ripple">
            <div></div>
            <div></div>
        </div>
    </div>
    <header class="<?php if (is_page('service')) {
      echo 'header-service';
    } elseif (is_page('apply')) {
      echo 'header-apply';
    } ?>">
        <div class="big-container">
          <?php if (have_rows('the_header_menu', 'option')) {
            while (have_rows('the_header_menu', 'option')) {
              the_row(); ?>
                <nav class="nav-bar">
                  <?php if (have_rows('left_menu', 'option')) {
                    while (have_rows('left_menu', 'option')) {
                      the_row(); ?>
                        <div class="nav-bar-left">
                            <a class="nav-bar-brand" href="<?php echo site_url(); ?>">
                              <?php if (is_page('service')) {
                                ?>
                                  <img alt="Logo" src="<?php the_sub_field('dark_logo', 'option'); ?>">
                              <?php } else { ?>
                                  <img alt="Logo" src="<?php the_sub_field('white_logo', 'option'); ?>">
                              <?php }
                              ?>
                            </a>
                            
                            <!-- Left Menu for Desktop -->
                          <?php if (have_rows('menu_item')) {
                            while (have_rows('menu_item')) {
                              the_row(); ?>
                              <?php if (get_sub_field('is_it_has_a_dropdown')) { ?>
                                    <div class="nav-link mos-dropdown-parent  d-none d-lg-block">
                                      <?php $menu_item = get_sub_field('menu_item'); ?>
                                        <a data-hover="Learn" href="<?php echo $menu_item['url']; ?>"> <?php echo $menu_item['title']; ?> <i class="fas fa-caret-down"></i></a>
                                      <?php if (have_rows('dropdown_items')) {
                                        ?>
                                          <div class="mos-drop-down">
                                              <div class="mos-drop-down-content">
                                                <?php
                                                while (have_rows('dropdown_items')) {
                                                  the_row();
                                                  if (!empty(get_sub_field('dropdown_item'))) {
                                                    $item = get_sub_field('dropdown_item');
                                                  } else {
                                                    $item = 0;
                                                  }
                                                  ?>
                                                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                                
                                                <?php } ?>
                                              </div>
                                          </div>
                                      <?php } ?>
                                    </div>
                              <?php } else { ?>
                                    <div class="nav-link mos-dropdown-parent  d-none d-lg-block">
                                        <div class="nav-bar-right">
                                          <?php $menu_item = get_sub_field('menu_item'); ?>
                                            <a class="nav-link" href="<?php echo $menu_item['url']; ?>">
                                              <?php echo $menu_item['title']; ?>
                                            </a>
                                        </div>
                                    </div>
                                <?php
                              }
                            }
                          }
                          ?>
                            <a>
                        </div>
                    <?php }
                  } ?>
                    <!-- Right Desktop Menu -->
                  <?php if (have_rows('right_menu')){
                  while (have_rows('right_menu')){
                  the_row();
                  ?>
                    <div class="nav-bar-right d-none d-lg-flex">
                      <?php if (have_rows('menu_item', 'option')) {
                        while (have_rows('menu_item', 'option')) {
                          the_row();
                          ?>
                          <?php if (get_sub_field('is_it_has_a_dropdown')) { ?>
                                <div class="nav-link mos-dropdown-parent">
                                  <?php $menu_item = get_sub_field('menu_item'); ?>
                                    <a data-hover="Learn" href="<?php echo $menu_item['url']; ?>"> <?php echo $menu_item['title']; ?> <i class="fas fa-caret-down"></i></a>
                                  <?php if (have_rows('dropdown_items')) { ?>
                                      <div class="mos-drop-down">
                                          <div class="mos-drop-down-content">
                                            <?php
                                            while (have_rows('dropdown_items')) {
                                              the_row();
                                              $item = get_sub_field('dropdown_item');
                                              ?>
                                                <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                            <?php }
                                            ?>
                                          </div>
                                      </div>
                                    <?php
                                  } ?>
                                
                                </div>
                          <?php } else {
                            ?>
                            <?php $item = get_sub_field('menu_item'); ?>
                                <!-- Single Menu right -->
                                <a class="nav-link" href="<?php echo $item['url']; ?>"><?php ?><?php echo $item['title']; ?></a>
                          <?php } ?>
                        <?php }
                      } ?>
                      
                      <?php }
                      } ?>
                        <a class="nav-link call-nav-link" href="tel:<?php the_sub_field('phone_no'); ?>">
                          <?php if (is_page('service')): ?>
                              <i class="fal fa-phone"></i><?php the_sub_field('phone_no'); ?>
                          <?php elseif (is_front_page()): ?>
                              <i class="fas fa-phone fa-rotate-90"></i><?php the_sub_field('phone_no'); ?>
                          <?php elseif (is_page('apply')): ?>
                              <i class="fas fa-phone fa-rotate-90"></i><?php the_sub_field('phone_no'); ?>
                          <?php endif; ?>
                        </a>
                      <?php $book_button = get_sub_field('book_button'); ?>
                        <a class="header-btn" target="<?php echo $book_button['target']; ?>" href="<?php echo $book_button['url']; ?>"><?php echo $book_button['title']; ?></a>
                    </div>
                    <!-- END Desktop Menu -->
                    
                    <!-- Menu for Mobile -->
                    <div class="navbar-collapse-wrapper d-lg-none">
                        <div class="navbar-collapse-toggler">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    
                    <a class="header-btn d-lg-none" href="#">Book Now</a>
                    <div class="nav-bar-right-mobile d-lg-none d-flex">
                        <div class="mobile-menu-logo-container">
                            <a class="nav-bar-brand" href="<?php echo site_url(); ?>">
                            <img class="mobile-menu-logo" src="<?php echo esc_url(get_sub_field('mobile_menu_logo')['url']);?>" alt="<?php esc_attr_e(get_sub_field('mobile_menu_logo')['alt']);?>">
                            </a>
                            <img class="mobile-menu-close" src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png" alt="close">
                        </div>
                      <?php if (have_rows('left_menu')) {
                        while (have_rows('left_menu')) {
                          the_row();
                          ?>
                          <?php if (have_rows('menu_item')) {
                            while (have_rows('menu_item')) {
                              the_row();
                              ?>
                              <?php if (get_sub_field('is_it_has_a_dropdown')) {
                                ?>
                                    <div class="nav-link mos-dropdown-parent">
                                      <?php $menu_item = get_sub_field('menu_item'); ?>
                                        <a data-hover="Learn" href="<?php echo $menu_item['url']; ?>"><?php echo $menu_item['title']; ?><i class="fas fa-caret-down"></i></a>
                                      <?php if (have_rows('dropdown_items')) {
                                        ?>
                                          <div class="mos-drop-down">
                                              <div class="mos-drop-down-content">
                                                <?php
                                                while (have_rows('dropdown_items')) {
                                                  the_row();
                                                  $item = get_sub_field('dropdown_item');
                                                  ?>
                                                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                                <?php } ?>
                                              </div>
                                          </div>
                                      <?php } ?>
                                    </div>
                              <?php } else {
                                $item = get_sub_field('menu_item');
                                ?>
                                    <a class="nav-link" href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                <?php
                              } ?>
                            
                            <?php }
                          } ?>
                        
                        <?php }
                      } ?>
                      <?php if (have_rows('right_menu')) {
                        while (have_rows('right_menu')) {
                          the_row();
                          ?>
                          <?php if (have_rows('menu_item')) {
                            while (have_rows('menu_item')) {
                              the_row();
                              ?>
                              <?php if (get_sub_field('is_it_has_a_dropdown')) {
                                ?>
                                    <div class="nav-link mos-dropdown-parent">
                                      <?php $menu_item = get_sub_field('menu_item'); ?>
                                        <a data-hover="Learn" href="<?php echo $menu_item['url']; ?>"><?php echo $menu_item['title']; ?><i class="fas fa-caret-down"></i></a>
                                      <?php if (have_rows('dropdown_items')) {
                                        ?>
                                          <div class="mos-drop-down">
                                              <div class="mos-drop-down-content">
                                                <?php
                                                while (have_rows('dropdown_items')) {
                                                  the_row();
                                                  $item = get_sub_field('dropdown_item');
                                                  ?>
                                                    <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                                <?php } ?>
                                              </div>
                                          </div>
                                      <?php } ?>
                                    </div>
                              <?php } else {
                                $item = get_sub_field('menu_item');
                                ?>
                                    <a class="nav-link" href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                <?php
                              } ?>
                            
                            <?php }
                          } ?>
                        
                        <?php }
                      } ?>
                        <a class="nav-link call-nav-link" href="tel:<?php the_sub_field('phone_no'); ?>">
                            <span><?php _e('Questions? Give us a call!') ?></span>
                            <br>
                          <?php the_sub_field('phone_no'); ?>
                        </a>
                    </div>
                
                </nav>
            <?php }
          } ?>
        </div>
        <!-- END Menu for Mobile -->
    </header>
