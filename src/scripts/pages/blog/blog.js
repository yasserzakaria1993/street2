import 'isotope-layout';
import imagesloaded from 'imagesloaded';
import 'jquery-bridget';

export const blog = () => {
  jqueryBridge('isotope', isotope, $);
  jqueryBridge('imagesloaded', imagesloaded, $);
  const $blogFeeds = $('section.blog-feeds .row');
  if ($blogFeeds.length) {
    $blogFeeds.imagesloaded(() => {
      // console.log('imagesloaded');
      $blogFeeds.isotope({
        itemSelector: 'section.blog-feeds .isotope-element',
        // layoutMode: 'masonry',
        filter: '*',
        resize: true,
        stamp: '.news-letter',
        masonry: {
          percentPosition: true,
          columnWidth: 'section.blog-feeds .isotope-element',
          horizontalOrder: true,
        },
      });
      
    });
    const $scrollToThis = $('[scroll-to-this]');
    $('section.blog-filter .filter').on('click', '.filter-item', function () {
      $(this).addClass('active').siblings().removeClass('active');
      $blogFeeds.isotope({filter: $(this).attr('data-filter')});
      $('html, body').animate({scrollTop: $scrollToThis.offset().top - 50}, 500);
    });
  }
};