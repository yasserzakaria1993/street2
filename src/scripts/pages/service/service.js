// $('.services-tabs button').click(function(){
//   let _val = $(this).data('index');
//   console.log(_val);
//   $('table.services-table').each(function(){
//     $('table.services-table').removeClass('active');
//     if (_val === $(this).data('index')){
//       console.log($(this).data('index'));
//      $(this).addClass('active');
//     }
//   });
// });

// Add click event to all cards
$('.services-tabs .the-item').click(function () {
    
    // Remove previous selection
    $('table.services-table').removeClass('active');
    
    // Get data-index from clicked .card
    let dataIndex = $(this).attr('data-index');
    
    // Add active class to corresponding over-menu
    $('table.services-table[data-index=' + dataIndex + ']').addClass('active');
    
    // Add selected class to the card
    $('.services-tabs .the-item').removeClass('active');
    $(this).addClass('active');
    
});
$('.services-tabs').on('change', function () {
    
    // Remove previous selection
    $('table.services-table').removeClass('active');
    
    // Get data-index from clicked .card
    let dataIndex = $(this).val();
    
    // Add active class to corresponding over-menu
    $('table.services-table[data-index=' + dataIndex + ']').addClass('active');
    
    // Add selected class to the card
    $('.services-tabs .the-item').removeClass('active');
    $(this).addClass('active');
    
});


let allowed= true;
let $progress_truck_flow = $('.progress-truck-flow');
// let $active_line = $('.active-line');
let $circle_truck_flow = $('.progress-truck-flow .circle-truck-flow');
let $movableElements = $('.non-active-line,.circle-truck-flow');
let positions = [0, 32, 65, 94.69];
let currentActive = 1;

function goToNearestTruck() {
    let distances = positions.map(position => Math.abs(position - $movableElements.position().left / $movableElements.parent().width() * 100));
    console.log(distances);
    let newActive = distances.indexOf(Math.min(...distances));
    if (currentActive !== newActive) {
        truckSizesArray[newActive].trigger('click');
        currentActive = newActive;
    }else{
        $movableElements.css('left', positions[newActive] + '%');
    }
}

let truckSizesArray = [
    $('.truck-sizes-flow .m-truck').on('click', function () {
        allowed && $movableElements.css('left', positions[0] + '%');
    }),
    $('.truck-sizes-flow .l-truck').on('click', function () {
        allowed && $movableElements.css('left', positions[1] + '%');
    }),
    $('.truck-sizes-flow .xl-truck').on('click', function () {
        allowed && $movableElements.css('left', positions[2] + '%');
    }),
    $('.truck-sizes-flow .xxl-truck').on('click', function () {
        allowed && $movableElements.css('left', positions[3] + '%');
    }),
];

function getZoom() {
    let width = $(window).outerWidth();
    return width > 1500 ? 1 :
        width > 1350 ? 0.9 :
            width > 1200 ? 0.8 : 0.7;
}


function mouseMoveHandler(e) {
    let position =(e.offsetX-27)/getZoom() / $(this).outerWidth() * 100;
    console.log(position, e.offsetX,$(this).outerWidth());
    $movableElements.css('left', Math.max(0,position)+ '%');
}

$progress_truck_flow.on('mousedown', (e) => {
    if (allowed) {
        console.log('mousedown');
        mouseMoveHandler.apply($progress_truck_flow[0],[e]);
        $movableElements.addClass('dragging');
        $($progress_truck_flow).on('mousemove',mouseMoveHandler);
    }
});
$(window).on('mouseup', () => {
    if($movableElements.hasClass('dragging')){
        console.log('mouseup');
        $movableElements.removeClass('dragging');
        $($progress_truck_flow).off('mousemove', mouseMoveHandler);
        goToNearestTruck();
    }
});


$('.truck-sizes-flow .truck-size').on('click', function () {
    if (allowed) {
        allowed=false;
        $('.truck-sizes-flow .truck-size').removeClass('active');
        $(this).addClass('active');
        // Remove previous selection
        // $(".truck-size-result-item").slideUp('slow').removeClass('active');
    
        // Get data-index from clicked .card
    
        let dataIndex = $(this).attr('data-index');
    
        $('.truck-size-result-item.active').removeClass('active').slideUp(500, function () {
            // Add active class to corresponding over-menu
            $('.truck-size-result-item[data-index=' + dataIndex + ']').slideDown(500, function () {
                $(this).addClass('active');
                allowed=true;
            });
        });
    }
    
    
});

$('.select-truck-size').on('change', function () {
    
    // Remove previous selection
    $('.truck-size-result-item').removeClass('active');
    
    // Get data-index from clicked .card
    let dataIndex = $(this).val();
    
    // Add active class to corresponding over-menu
    $('.truck-size-result-item[data-index=' + dataIndex + ']').addClass('active');
    
});


$('.truck-sizes-tabs .the-item').click(function () {
    
    // Remove previous selection
    $('.truck-sizes-content').removeClass('active');
    
    // Get data-index from clicked .card
    let dataIndex = $(this).attr('data-index');
    
    // Add active class to corresponding over-menu
    $('.truck-sizes-content[data-index=' + dataIndex + ']').addClass('active');
    
    // Add selected class to the card
    $('.truck-sizes-tabs .the-item').removeClass('active');
    $(this).addClass('active');
    
});
$('.truck-sizes-tabs').on('change', function () {
    
    // Remove previous selection
    $('.truck-sizes-content').removeClass('active');
    
    // Get data-index from clicked .card
    let dataIndex = $(this).val();
    
    // Add active class to corresponding over-menu
    $('.truck-sizes-content[data-index=' + dataIndex + ']').addClass('active');
    
    // Add selected class to the card
    $('.truck-sizes-tabs .the-item').removeClass('active');
    $(this).addClass('active');
    
});


$('.rounded-service-wrapper').each(function () {
    let the_color = $(this).attr('data-color');
    let the_shape = $(this).find('.rounded-shape');
    let the_btn = $(this).find('.get-an-estimate');
    
    the_shape.css('background', the_color);
    the_btn.css('background', the_color);
    the_btn.css('border-color', the_color);
});