<?php
/**
 * The main template file
 *
 * Template Name: Service
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Move_Street
 */

get_header();
?>
  <div class="service-page-wrapper">
		<?php if (have_rows('layout')): ?>
			<?php while (have_rows('layout')): ?>
				<?php the_row(); ?>
				
				<?php if (get_row_layout() == 'first_section'): ?>
          <section class="main-section" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/service/hero_img.png);">
            <div class="container">
              <div class="main-section-content">
                <div class="row">
                  <div class="col-12 col-lg-7">
                    <h2 class="mos-title-1"><?php the_sub_field('section_title'); ?>
                      <span class="primary-color">.</span></h2>
                    <p class="mos-paragraph">
											<?php the_sub_field('section_description'); ?>
                    </p>
										<?php if (have_rows('features')): ?>
                      <div class="features">
                        <div class="row">
													<?php
													while (have_rows('features')):
														the_row(); ?>
                            <div class="col-md-6">
                              <div class="feature">
                                <div class="feature-icon"><i class="fal fa-check"></i></div>
                                <p class="feature-name"><?php the_sub_field('feature_title'); ?></p>
                              </div>
                            </div>
													<?php endwhile; ?>
                        </div>
                      </div>
										<?php endif; ?>
                  
                  </div>
                  <div class="col-12 col-lg-5  d-lg-flex d-none">
                    <div class="service-section-form">
                      <h3 class="text-center form-title">Request a Booking</h3>
                      <div class="input-group-wrapper">
												<?php
												$form_id = '5';
												gravity_form($form_id, false, false, false, '', true, 5);
												?>
                      </div>
                      <!--              <div class="input-group-wrapper">-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <input placeholder="Pick-up Postcode" type="text"/>-->
                      <!--                </div>-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <select>-->
                      <!--                    <option value="1">Pick-up Property Type</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                  </select>-->
                      <!--                </div>-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <select>-->
                      <!--                    <option value="1">Property Size</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                    <option value="1">asdas</option>-->
                      <!--                  </select>-->
                      <!--                </div>-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <input placeholder="Drop-off Postcode" type="text"/>-->
                      <!--                </div>-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <input placeholder="Email" type="text"/>-->
                      <!--                </div>-->
                      <!--                <div class="input-wrapper">-->
                      <!--                  <input placeholder="Mobile (optional)" type="text"/>-->
                      <!--                </div>-->
                      <!--              </div>-->
                      <!--              <button class="view-price-btn main-btn" type="submit">VIEW PRICE</button>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <svg preserveAspectRatio="none" viewBox="0 0 1 1">
              <path d="M0 1 L.863 0 L1 .41 V1H0Z" fill="#04c4d9"></path>
            </svg>
          </section>
				<?php elseif (get_row_layout() == 'how_much_section'): ?>
          <section class="how-much-section">
            <div class="how-much-section-content big-container">
              <div class="container">
                <div class="row d-lg-none d-flex">
                  <div class="col-12 col-lg-5">
                    <div class="service-section-form">
                      <h3 class="text-center form-title">Request a Booking</h3>
                      <!--              <div class="input-group-wrapper">-->
                      <!--	              --><?php
											//	              $form_id = '5';
											//	              gravity_form($form_id, false, false, false, '', true, 15);
											//	              ?>
                      <!--              </div>-->
                      <div class="input-group-wrapper">
                        <div class="input-wrapper">
                          <input placeholder="Pick-up Postcode" type="text"/>
                        </div>
                        <div class="input-wrapper">
                          <select>
                            <option value="1">Pick-up Property Type</option>
                            <option value="1">asdas</option>
                            <option value="1">asdas</option>
                            <option value="1">asdas</option>
                          </select>
                        </div>
                        <div class="input-wrapper">
                          <select>
                            <option value="1">Property Size</option>
                            <option value="1">asdas</option>
                            <option value="1">asdas</option>
                            <option value="1">asdas</option>
                          </select>
                        </div>
                        <div class="input-wrapper">
                          <input placeholder="Drop-off Postcode" type="text"/>
                        </div>
                        <div class="input-wrapper">
                          <input placeholder="Email" type="text"/>
                        </div>
                        <div class="input-wrapper">
                          <input placeholder="Mobile (optional)" type="text"/>
                        </div>
                      </div>
                      <button class="view-price-btn main-btn" type="submit">VIEW PRICE</button>
                    </div>
                  </div>
                </div>
                <h2 class="mos-title-1"><?php the_sub_field('section_title'); ?></h2>
                <p class="the-sub-title">
									<?php the_sub_field('section_small_description'); ?>
                </p>
              </div>
              <div class="container d-none d-lg-block">
                <div class="truck-sizes-flow">
									<?php if (have_rows('tab_1')) {
										while (have_rows('tab_1')) {
											the_row();
											?>
                      <div class="truck-size m-truck" data-index="1">
                        <h3 class="the-title"><?php the_sub_field('title_for_filter_bar'); ?></h3>
                        <h4 class="movers-count"><?php the_sub_field('subtitle_for_filter_bar'); ?></h4>
                      </div>
											<?php
										}
									}
									?>
									<?php if (have_rows('tab_2')) {
										while (have_rows('tab_2')) {
											the_row();
											?>
                      <div class="truck-size l-truck active" data-index="2">
                        <h3 class="the-title"><?php the_sub_field('title_for_filter_bar'); ?></h3>
                        <h4 class="movers-count"><?php the_sub_field('subtitle_for_filter_bar'); ?></h4>
                      </div>
											<?php
										}
									}
									?>
									<?php if (have_rows('tab_3')) {
										while (have_rows('tab_3')) {
											the_row();
											?>
                      <div class="truck-size xl-truck" data-index="3">
                        <h3 class="the-title"><?php the_sub_field('title_for_filter_bar'); ?></h3>
                        <h4 class="movers-count"><?php the_sub_field('subtitle_for_filter_bar'); ?></h4>
                      </div>
											<?php
										}
									}
									?>
									<?php if (have_rows('tab_4')) {
										while (have_rows('tab_4')) {
											the_row();
											?>
                      <div class="truck-size xxl-truck" data-index="4">
                        <h3 class="the-title"><?php the_sub_field('title_for_filter_bar'); ?></h3>
                        <h4 class="movers-count"><?php the_sub_field('subtitle_for_filter_bar'); ?></h4>
                      </div>
											<?php
										}
									}
									?>
                
                </div>
                <div class="progress-truck-flow">
                  <div class="the-line active-line">
                      <div class="the-line non-active-line"></div>
                  </div>
                  <div class="circle-truck-flow">
                    <div class="small-circle"></div>
                    <div class="the-triangle"></div>
                  </div>
                </div>
              </div>
              <div class="truck-size-results-wrapper d-none d-lg-block">
                <!-- Tab 1 Desktop -->
								<?php if (have_rows('tab_1')):
									while (have_rows('tab_1')):
										the_row();
										?>
                    <div class="truck-size-result-item" data-index="1">
                      <div class="row justify-content-between">
                        <div class="col-lg-3">
                          <div class="content-left">
                            <img alt="" src="<?php the_sub_field('left_image'); ?>">
                            <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                            <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
														<?php if (have_rows('left_content')):
															while (have_rows('left_content')):
																the_row();
																?>
                                <div class="Suitable-wrapper">
																	<?php $points_title = get_sub_field('points_title'); ?>
																	<?php if ($points_title) {
																		echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																	} ?>
                                  <div class="the-content">
																		
																		<?php
																		if (have_rows('points')):
																			while (have_rows('points')):
																				the_row();
																				?>
                                        <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																			<?php endwhile; endif; ?>
                                  </div>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')):
															while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
									<?php endwhile; endif; ?>
                <!-- END Tab 1 desktop -->
                <!-- Tab 2 Desktop -->
								<?php if (have_rows('tab_2')):
									while (have_rows('tab_2')):
										the_row();
										?>
                    <div class="truck-size-result-item active" data-index="2">
                      <div class="row justify-content-between">
                        <div class="col-lg-3">
                          <div class="content-left">
                            <img alt="" src="<?php the_sub_field('left_image'); ?>">
                            <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                            <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
														<?php if (have_rows('left_content')):
															while (have_rows('left_content')):
																the_row();
																?>
                                <div class="Suitable-wrapper">
																	<?php $points_title = get_sub_field('points_title'); ?>
																	<?php if ($points_title) {
																		echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																	} ?>
                                  <div class="the-content">
																		
																		<?php
																		if (have_rows('points')):
																			while (have_rows('points')):
																				the_row();
																				?>
                                        <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																			<?php endwhile; endif; ?>
                                  </div>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')):
															while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
									<?php endwhile; endif; ?>
                <!-- END Tab 2 Desktop -->
                <!-- Tab 3 Desktop -->
								<?php if (have_rows('tab_3')):
									while (have_rows('tab_3')):
										the_row();
										?>
                    <div class="truck-size-result-item" data-index="3">
                      <div class="row justify-content-between">
                        <div class="col-lg-3">
                          <div class="content-left">
                            <img alt="" src="<?php the_sub_field('left_image'); ?>">
                            <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                            <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
														<?php if (have_rows('left_content')):
															while (have_rows('left_content')):
																the_row();
																?>
                                <div class="Suitable-wrapper">
																	<?php $points_title = get_sub_field('points_title'); ?>
																	<?php if ($points_title) {
																		echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																	} ?>
                                  <div class="the-content">
																		
																		<?php
																		if (have_rows('points')):
																			while (have_rows('points')):
																				the_row();
																				?>
                                        <p><span class="hot-dot">•  </span><?php the_sub_field('point_title'); ?></p>
																			<?php endwhile; endif; ?>
                                  </div>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')):
															while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                    <i class="fal fa-check the-icon"></i>
                                    <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div><?php endif; ?>
																
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
									<?php endwhile; endif; ?>
                <!-- END Tab 3 Desktop -->
                <!-- Tab 4 Desktop -->
								<?php if (have_rows('tab_4')):
									while (have_rows('tab_4')):
										the_row();
										?>
                    <div class="truck-size-result-item" data-index="4">
                      <div class="row justify-content-between">
                        <div class="col-lg-3">
                          <div class="content-left">
                            <img alt="" src="<?php the_sub_field('left_image'); ?>">
                            <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                            <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
														<?php if (have_rows('left_content')):
															while (have_rows('left_content')):
																the_row();
																?>
                                <div class="Suitable-wrapper">
																	<?php $points_title = get_sub_field('points_title'); ?>
																	<?php if ($points_title) {
																		echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																	} ?>
                                  <div class="the-content">
																		
																		<?php
																		if (have_rows('points')):
																			while (have_rows('points')):
																				the_row();
																				?>
                                        <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																			<?php endwhile; endif; ?>
                                  </div>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')):
															while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      </div>
                    </div>
									<?php endwhile; endif; ?>
                <!-- END Tab 4 Desktop -->
              </div>
              <!-- Trucks For Mobile -->
              <div class="truck-size-results-wrapper-mobile d-block d-lg-none">
                <div class="container">
                  <select class="select-truck-size" id="" name="">
                    <option value="">Select an option to see rates</option>
										<?php if (have_rows('tab_1')) {
											while (have_rows('tab_1')) {
												the_row();
												?>
                        <option value="1"><?php the_sub_field('title_for_filter_bar');
													the_sub_field('subtitle_for_filter_bar'); ?></option>
											<?php }
										} ?>
										<?php if (have_rows('tab_2')) {
											while (have_rows('tab_2')) {
												the_row();
												?>
                        <option value="2"><?php the_sub_field('title_for_filter_bar');
													the_sub_field('subtitle_for_filter_bar'); ?></option>
											<?php }
										} ?>
										<?php if (have_rows('tab_3')) {
											while (have_rows('tab_3')) {
												the_row();
												?>
                        <option value="3"><?php the_sub_field('title_for_filter_bar');
													the_sub_field('subtitle_for_filter_bar'); ?></option>
											<?php }
										} ?>
										<?php if (have_rows('tab_4')) {
											while (have_rows('tab_4')) {
												the_row();
												?>
                        <option value="4"><?php the_sub_field('title_for_filter_bar');
													the_sub_field('subtitle_for_filter_bar'); ?></option>
											<?php }
										} ?>
                  </select>
                  <!-- Tab for Mobile -->
									<?php if (have_rows('tab_1')) {
										while (have_rows('tab_1')) {
											the_row();
											?>
                      <div class="truck-size-result-item" data-index="1">
                        <div class="top-text">
                          <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                          <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
                        </div>
                        <div class="bottom-section">
                          <div class="the-triangle"></div>
                          <img alt="" class="truck-img" src="<?php the_sub_field('left_image'); ?>">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          
                          </div>
													<?php if (have_rows('left_content')):
														while (have_rows('left_content')):
															the_row();
															?>
                              <div class="Suitable-wrapper">
																<?php $points_title = get_sub_field('points_title'); ?>
																<?php if ($points_title) {
																	echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																} ?>
                                <div class="the-content">
																	
																	<?php
																	if (have_rows('points')):
																		while (have_rows('points')):
																			the_row();
																			?>
                                      <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																		<?php endwhile; endif; ?>
                                </div>
                              </div>
														<?php endwhile; endif; ?>
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')): ?>
															<?php while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      
                      </div>
										<?php }
									} ?>
                  <!-- End tab for Mobile -->
                  <!-- Tab for Mobile -->
									<?php if (have_rows('tab_2')) {
										while (have_rows('tab_2')) {
											the_row();
											?>
                      <div class="truck-size-result-item active" data-index="2">
                        <div class="top-text">
                          <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                          <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
                        </div>
                        <div class="bottom-section">
                          <div class="the-triangle"></div>
                          <img alt="" class="truck-img" src="<?php the_sub_field('left_image'); ?>">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          
                          </div>
													<?php if (have_rows('left_content')):
														while (have_rows('left_content')):
															the_row();
															?>
                              <div class="Suitable-wrapper">
																<?php $points_title = get_sub_field('points_title'); ?>
																<?php if ($points_title) {
																	echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																} ?>
                                <div class="the-content">
																	
																	<?php
																	if (have_rows('points')):
																		while (have_rows('points')):
																			the_row();
																			?>
                                      <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																		<?php endwhile; endif; ?>
                                </div>
                              </div>
														<?php endwhile; endif; ?>
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')): ?>
															<?php while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      
                      </div>
										<?php }
									} ?>
                  <!-- End tab for Mobile -->
                  
                  <!-- Tab for Mobile -->
									<?php if (have_rows('tab_3')) {
										while (have_rows('tab_3')) {
											the_row();
											?>
                      <div class="truck-size-result-item" data-index="3">
                        <div class="top-text">
                          <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                          <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
                        </div>
                        <div class="bottom-section">
                          <div class="the-triangle"></div>
                          <img alt="" class="truck-img" src="<?php the_sub_field('left_image'); ?>">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          
                          </div>
													<?php if (have_rows('left_content')):
														while (have_rows('left_content')):
															the_row();
															?>
                              <div class="Suitable-wrapper">
																<?php $points_title = get_sub_field('points_title'); ?>
																<?php if ($points_title) {
																	echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																} ?>
                                <div class="the-content">
																	
																	<?php
																	if (have_rows('points')):
																		while (have_rows('points')):
																			the_row();
																			?>
                                      <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																		<?php endwhile; endif; ?>
                                </div>
                              </div>
														<?php endwhile; endif; ?>
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')): ?>
															<?php while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      
                      </div>
										<?php }
									} ?>
                  
                  <!-- End tab for Mobile -->
                  
                  <!-- Tab for Mobile -->
									<?php if (have_rows('tab_4')) {
										while (have_rows('tab_4')) {
											the_row();
											?>
                      <div class="truck-size-result-item" data-index="4">
                        <div class="top-text">
                          <h2 class="the-main-title"><?php the_sub_field('left_title'); ?></h2>
                          <h3 class="the-price"><?php the_sub_field('left_price'); ?></h3>
                        </div>
                        <div class="bottom-section">
                          <div class="the-triangle"></div>
                          <img alt="" class="truck-img" src="<?php the_sub_field('left_image'); ?>">
                          <div class="right-content">
                            <h2 class="the-title"><?php the_sub_field('right_title'); ?></h2>
														<?php if (have_rows('right_table')):
															while (have_rows('right_table')):
																the_row(); ?>
                                <div class="day-cost">
                                  <h4 class="text"><?php the_sub_field('item_title'); ?></h4>
                                  <h5 class="value"><?php the_sub_field('item_value'); ?></h5>
                                </div>
															<?php endwhile; endif; ?>
                          
                          </div>
													<?php if (have_rows('left_content')):
														while (have_rows('left_content')):
															the_row();
															?>
                              <div class="Suitable-wrapper">
																<?php $points_title = get_sub_field('points_title'); ?>
																<?php if ($points_title) {
																	echo '
                   <h4 class="the-title">Suitable for all this:</h4>
                   ';
																} ?>
                                <div class="the-content">
																	
																	<?php
																	if (have_rows('points')):
																		while (have_rows('points')):
																			the_row();
																			?>
                                      <p><span class="hot-dot">• </span><?php the_sub_field('point_title'); ?></p>
																		<?php endwhile; endif; ?>
                                </div>
                              </div>
														<?php endwhile; endif; ?>
                          <div class="center-content">
                            <h2 class="top-title"><?php the_sub_field('center_title'); ?></h2>
                            <h2 class="sub-title"><?php the_sub_field('center_sub_title'); ?></h2>
														<?php if (have_rows('cost_table')): ?>
															<?php while (have_rows('cost_table')):
																the_row(); ?>
																<?php if (have_rows('first_table_row')):
																while (have_rows('first_table_row')):
																	the_row(); ?>
                                  <div class="the-item price-per-hour">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('second_table_row')):
																while (have_rows('second_table_row')):
																	the_row(); ?>
                                  <div class="the-item hours-reserved">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (have_rows('third_table_row')):
																while (have_rows('third_table_row')):
																	the_row(); ?>
                                  <div class="the-item travel-time">
                                    <h4 class="text"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="value"><?php the_sub_field('value'); ?></h5>
                                  </div>
																<?php endwhile; endif; ?>
																<?php if (get_sub_field('total_cost')): ?>
                                <div class="the-item total-cost">
                                  <h4 class="text">Total Cost</h4>
                                  <h5 class="value"><?php the_sub_field('total_cost'); ?></h5>
                                </div>
															<?php endif; ?>
																<?php if (have_rows('points_after_table')): ?>
																<?php while (have_rows('points_after_table')): ?>
																	<?php the_row(); ?>
                                  <div class="verified-wrapper">
																	<?php if (get_sub_field('point_1_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_1_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																	<?php if (get_sub_field('point_2_title')): ?>
                                    <div class="verified">
                                      <i class="fal fa-check the-icon"></i>
                                      <h4 class="the-text"><?php the_sub_field('point_2_title'); ?></h4>
                                    </div>
																	<?php endif; ?>
																<?php endwhile; endif; ?>
                                </div>
															<?php endwhile; endif; ?>
                          </div>
                        </div>
                      
                      </div>
										<?php }
									} ?>
                  <!-- End tab for Mobile -->
                </div>
              </div>
              <!-- END Trucks -->
            </div>
          </section>
				<?php elseif (get_row_layout() == 'time_estimates_section'): ?>
          <section class="services-table-section">
            <div class="container">
              <div class="services-table-section-content">
                <h2 class="the-title"><?php the_sub_field('section_title'); ?></h2>
								<?php if (have_rows('choices')):
									while (have_rows('choices')):
										the_row(); ?>
                    
                    <div class="services-tabs d-none d-lg-flex">
                      <button class="the-item active" data-index="1"><?php the_sub_field('first_choice'); ?></button>
                      <button class="the-item" data-index="2"><?php the_sub_field('second_choice'); ?></button>
                      <button class="the-item" data-index="3"><?php the_sub_field('third_choice'); ?></button>
                    </div>
                    <select class="services-tabs d-block d-lg-none">
                      <option class="the-item" data-index="1" value="1"><?php the_sub_field('first_choice'); ?></option>
                      <option class="the-item" data-index="2" value="2"><?php the_sub_field('second_choice'); ?></option>
                      <option class="the-item" data-index="3" value="3"><?php the_sub_field('third_choice'); ?></option>
                    </select>
									<?php endwhile; endif; ?>
								<?php if (have_rows('time_estimates_table')):
									while (have_rows('time_estimates_table')):
										the_row();
										?>
                    <div class="overflow-x">
											<?php if (have_rows('table_1')):
												while (have_rows('table_1')):
													the_row(); ?>
                          <table class="active services-table" data-index="1">
                            <tr>
															<?php if (have_rows('table_header')):
															while (have_rows('table_header')):
															the_row();
															?>
                              <th><?php the_sub_field('left_column'); ?></th>
                              <th><?php the_sub_field('center_column'); ?></th>
                              <th><?php the_sub_field('right_column'); ?></th>
                            </tr>
														<?php endwhile;
														endif; ?>
														<?php if (have_rows('table_rows')):
															while (have_rows('table_rows')):
																the_row();
																?>
                                <tr>
                                  <td><?php the_sub_field('left_column'); ?></td>
                                  <td><?php the_sub_field('center_column'); ?></td>
                                  <td><?php the_sub_field('right_column'); ?></td>
                                </tr>
															<?php endwhile; endif; ?>
														<?php if (have_rows('table_footer')):
															while (have_rows('table_footer')):
																the_row();
																
																?>
                                <tr class="last-row">
                                  <!-- Left Text -->
                                  <td><?php the_sub_field('left_text'); ?></td>
                                  <td><?php the_sub_field('right_text'); ?></td>
                                  <td></td>
                                </tr>
															<?php endwhile; endif; ?>
                          </table>
												<?php endwhile; endif; ?>
                      
                      <!-- Second Table -->
											<?php if (have_rows('table_2')):
												while (have_rows('table_2')):
													the_row(); ?>
                          <table class="services-table" data-index="2">
                            <tr>
															<?php if (have_rows('table_header')):
															while (have_rows('table_header')):
															the_row();
															?>
                              <th><?php the_sub_field('left_column'); ?></th>
                              <th><?php the_sub_field('center_column'); ?></th>
                              <th><?php the_sub_field('right_column'); ?></th>
                            </tr>
														<?php endwhile;
														endif; ?>
														<?php if (have_rows('table_rows')):
															while (have_rows('table_rows')):
																the_row();
																?>
                                <tr>
                                  <td><?php the_sub_field('left_column'); ?></td>
                                  <td><?php the_sub_field('center_column'); ?></td>
                                  <td><?php the_sub_field('right_column'); ?></td>
                                </tr>
															<?php endwhile; endif; ?>
														<?php if (have_rows('table_footer')):
															while (have_rows('table_footer')):
																the_row();
																?>
                                <tr class="last-row">
                                  <td><?php the_sub_field('left_text'); ?></td>
                                  <td><?php the_sub_field('right_text'); ?></td>
                                  <td></td>
                                </tr>
															<?php endwhile; endif; ?>
                          </table>
												<?php endwhile; endif; ?>
                      <!-- Third Table -->
											<?php if (have_rows('table_3')):
												while (have_rows('table_3')):
													the_row(); ?>
                          <table class="services-table" data-index="3">
                            <tr>
															<?php if (have_rows('table_header')):
															while (have_rows('table_header')):
															the_row();
															?>
                              <th><?php the_sub_field('left_column'); ?></th>
                              <th><?php the_sub_field('center_column'); ?></th>
                              <th><?php the_sub_field('right_column'); ?></th>
                            </tr>
														<?php endwhile;
														endif; ?>
														<?php if (have_rows('table_rows')):
															while (have_rows('table_rows')):
																the_row();
																?>
                                <tr>
                                  <td><?php the_sub_field('left_column'); ?></td>
                                  <td><?php the_sub_field('center_column'); ?></td>
                                  <td><?php the_sub_field('right_column'); ?></td>
                                </tr>
															<?php endwhile; endif; ?>
														<?php if (have_rows('table_footer')):
															while (have_rows('table_footer')):
																the_row();
																?>
                                <tr class="last-row">
                                  <td><?php the_sub_field('left_text'); ?></td>
                                  <td><?php the_sub_field('right_text'); ?></td>
                                  <td></td>
                                </tr>
															<?php endwhile; endif; ?>
                          </table>
												<?php endwhile; endif; ?>
                    </div>
									<?php endwhile; endif; ?>
              </div>
            </div>
          </section>
				<?php elseif (get_row_layout() == 'our_trucks_sizes'): ?>
          <section class="our-truck-sizes-section">
            <div class="container">
              <div class="our-truck-sizes-section-content">
                <h2 class="truck-sizes-title"><?php the_sub_field('section_title'); ?></h2>
                <p class="truck-sizes-text"><?php the_sub_field('section_subtitle'); ?></p>
								<?php if (have_rows('choices')):
									while (have_rows('choices')):
										the_row();
										?>
                    <div class="truck-sizes-tabs d-none d-lg-flex">
											<?php if (get_sub_field('first_choice')) { ?>
                        <button class="the-item active" data-index="1"><?php the_sub_field('first_choice'); ?></button><?php } ?>
											<?php if (get_sub_field('second_choice')) { ?>
                        <button class="the-item" data-index="2"><?php the_sub_field('second_choice'); ?></button><?php } ?>
											<?php if (get_sub_field('third_choice')) { ?>
                        <button class="the-item" data-index="3"><?php the_sub_field('third_choice'); ?></button><?php } ?>
                    </div>
                    
                    <select class="truck-sizes-tabs d-block d-lg-none">
											<?php if (get_sub_field('first_choice')) { ?>
                        <option class="the-item" data-index="1" value="1"><?php the_sub_field('first_choice'); ?></option><?php } ?>
											<?php if (get_sub_field('second_choice')) { ?>
                        <option class="the-item" data-index="2" value="2"><?php the_sub_field('second_choice'); ?></option><?php } ?>
											<?php if (get_sub_field('third_choice')) { ?>
                        <option class="the-item" data-index="3" value="3"><?php the_sub_field('third_choice'); ?></option><?php } ?>
                    </select>
									
									<?php endwhile; endif; ?>
								<?php if (have_rows('truck_tab')):
									while (have_rows('truck_tab')):
										the_row();
										?>
                    <div class="overflow-x">
                      
                      <!-- Truck Tab -->
											<?php if (have_rows('tab_1')):
												while (have_rows('tab_1')):
													the_row();
													?>
                          
                          <div class="active truck-sizes-content" data-index="1">
                            <h3 class="big-title"><?php the_sub_field('title'); ?></h3>
                            <p class="paragraph"><?php the_sub_field('description'); ?></p>
                            <img alt="" class="the-img" src="<?php the_sub_fielD('image'); ?>">
                            <div class="Suitable-wrapper">
															<?php if (have_rows('suitable_for')):
																?>
                                <h4 class="the-title">Suitable for all this:</h4>
                                <div class="the-content">
																	<?php
																	while (have_rows('suitable_for')):
																		the_row();
																		?>
                                    <p><span class="hot-dot">• </span><?php the_sub_field('point'); ?></p>
																	<?php endwhile; ?>
                                </div>
															<?php endif; ?>
                            </div>
                          </div>
												<?php endwhile; endif; ?>
                      <!-- END Tab -->
                      
                      <!-- Truck Tab -->
											<?php if (have_rows('tab_2')):
												while (have_rows('tab_2')):
													the_row();
													?>
                          
                          <div class="truck-sizes-content" data-index="2">
                            <h3 class="big-title"><?php the_sub_field('title'); ?></h3>
                            <p class="paragraph"><?php the_sub_field('description'); ?></p>
                            <img alt="" class="the-img" src="<?php the_sub_fielD('image'); ?>">
                            <div class="Suitable-wrapper">
															<?php if (have_rows('suitable_for')):
																?>
                                <h4 class="the-title">Suitable for all this:</h4>
                                <div class="the-content">
																	<?php
																	while (have_rows('suitable_for')):
																		the_row();
																		?>
                                    <p><span class="hot-dot">•</span><?php the_sub_field('point'); ?></p>
																	<?php endwhile; ?>
                                </div>
															<?php endif; ?>
                            </div>
                          </div>
												<?php endwhile; endif; ?>
                      <!-- END Tab -->
                      
                      
                      <!-- Truck Tab -->
											<?php if (have_rows('tab_3')):
												while (have_rows('tab_3')):
													the_row();
													?>
                          
                          <div class="truck-sizes-content" data-index="3">
                            <h3 class="big-title"><?php the_sub_field('title'); ?></h3>
                            <p class="paragraph"><?php the_sub_field('description'); ?></p>
                            <img alt="" class="the-img" src="<?php the_sub_fielD('image'); ?>">
                            <div class="Suitable-wrapper">
															<?php if (have_rows('suitable_for')):
																?>
                                <h4 class="the-title">Suitable for all this:</h4>
                                <div class="the-content">
																	<?php
																	while (have_rows('suitable_for')):
																		the_row();
																		?>
                                    <p><span class="hot-dot">•</span><?php the_sub_field('point'); ?></p>
																	<?php endwhile; ?>
                                </div>
															<?php endif; ?>
                            </div>
                          </div>
												<?php endwhile; endif; ?>
                      <!-- END Tab -->
                    
                    
                    </div>
									<?php endwhile; endif; ?>
              </div>
            </div>
          </section>
				<?php elseif (get_row_layout() == 'our_moving_services'): ?>
          <section class="clip-path-services-section">
            <svg preserveAspectRatio="none" viewBox="0 0 1 1">
              <path d="M0 1 V .42 L.141 0L1 1 H0Z" fill="#f6fafe"/>
            </svg>
            <div class="margin-top-negative">
              <h2 class="mos-title-1"><?php the_sub_field('section_title'); ?></h2>
							
							<?php if (have_rows('service')):
								while (have_rows('service')):
									the_row(); ?>
                  <div class="rounded-service-wrapper left-rounded" data-color="<?php the_sub_field('service_color'); ?>">
                    <div class="the-title-wrapper  d-block d-lg-none">
                      <h3 class="the-big-title"><?php the_sub_field('title'); ?></h3>
                      <h5 class="the-small-title"><?php the_sub_field('subtitle'); ?></h5>
                    </div>
                    <div class="the-image-wrapper">
                      <div class="rounded-shape right-shape"></div>
                      <img class="the-img" src="<?php the_sub_field('image'); ?>">
                    </div>
                    <div class="the-content">
                      <h3 class="the-big-title d-none d-lg-block"><?php the_sub_field('title'); ?></h3>
                      <h5 class="the-small-title d-none d-lg-block"><?php the_sub_field('subtitle'); ?></h5>
                      <p class="the-paragraph">
												<?php the_sub_field('description'); ?>
                      </p>
											<?php $button = get_sub_field('button'); ?>
											<?php if ($button) { ?><a class="get-an-estimate sub-btn" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a><?php } ?>
                    </div>
                  </div>
								<?php endwhile; endif; ?>
            
            </div>
          </section>
				<?php elseif (get_row_layout() == 'contact-form'): ?>
          <section class="ready-to-book-your-move">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-11 col-sm-10">
                  <h2 class="mos-title-2 text-center">
                    Ready to book your move?
                  </h2>
                  <h5 class="mos-paragraph text-center">
                    Schedule an appointment today.
                  </h5>
                </div>
                
                <form action="" class="footer-form">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12 col-lg-6">
                        <input placeholder="Pick-up Postcode" type="text">
                      </div>
                      <div class="col-md-12 col-lg-6">
                        <select>
                          <option value="1">Pick-up Property Type</option>
                          <option value="1">asdas</option>
                          <option value="1">asdas</option>
                          <option value="1">asdas</option>
                        </select>
                      </div>
                      <div class="col-md-12 col-lg-6">
                        <select>
                          <option value="1">Property Size</option>
                          <option value="1">asdas</option>
                          <option value="1">asdas</option>
                          <option value="1">asdas</option>
                        </select>
                      </div>
                      <div class="col-md-12 col-lg-6">
                        <input placeholder="Drop-off Postcode" type="text">
                      </div>
                      <div class="col-md-12 col-lg-6">
                        <input placeholder="Email" type="text">
                      </div>
                      <div class="col-md-12 col-lg-6">
                        <input placeholder="Mobile" type="text">
                      </div>
                    
                    </div>
                  </div>
                  <button class="main-btn" type="submit">VIEW PRICE</button>
                </form>
              </div>
            </div>
            <div class="building-img">
              <img alt="building-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/service/23-layers.png">
            </div>
          </section>
				<?php endif; ?>
			<?php endwhile; endif; ?>
  </div>

<?php
get_footer();
