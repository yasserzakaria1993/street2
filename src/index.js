import 'normalize.css/normalize.css';

import './styles/style.scss';
import '@fortawesome/fontawesome-pro/scss/fontawesome.scss';
import '@fortawesome/fontawesome-pro/scss/light.scss';
import '@fortawesome/fontawesome-pro/scss/regular.scss';
import '@fortawesome/fontawesome-pro/scss/solid.scss';
import '@fortawesome/fontawesome-pro/scss/brands.scss';
import 'slick-carousel/slick/slick.scss';
import 'select2/dist/css/select2.min.css';

import {header} from './scripts/layouts';
import {blog, home, apply} from './scripts/pages';
import {runAllHelpingFunctions, WayPoints} from './scripts/general';
import {SplitText, TweenMax} from 'gsap/all';


const reInvokableFunction = () => {
  runAllHelpingFunctions($(document));
  // header.init();
  home();
  apply();
  blog();
  $('.toggleable-slide-up-header').on('click', function () {
    $(this).parent().toggleClass('toggled');
    $(this).next('.toggleable-slide-up-body').slideToggle(400, function () {
      Waypoint.refreshAll();
      $(this).parent().toggleClass('active');
    });
    $(this).toggleClass('toggleable-slide-up-header-opened');
  });
  
  
  $( ".mos-dropdown-parent" ).on('mouseenter',function () {
    $(this).find('.mos-drop-down').slideDown(150);
  }).on('mouseleave',function () {
    $(this).find('.mos-drop-down').slideUp(150);
  });
  /** Change the arrow on the Select**/
  $('.select2-selection__arrow b').replaceWith('<i class="fas fa-caret-down"></i>');
  Waypoint.refreshAll();
};
// <i class="fal fa-plus"></i><i class="fal fa-minus"></i>

$(document).ready(() => {
  $(window).on('resize', () => {
    Waypoint.refreshAll();
    Waypoint.enableAll();
  });
  // SwupInit(reInvokableFunction);
  const $ivwp = $('[class ^="iv-wp"], [class *= " iv-wp"]');
  TweenMax.set($ivwp, {autoAlpha: 0});
  let filtered = $ivwp.filter('.iv-wp');
  if (filtered.length) {
    let mySplitText = new SplitText(filtered, {type: 'lines', linesClass: 'split-line'});
  }
  setTimeout(reInvokableFunction, 500);
  
});

$(window).on('load', function () {
  let $loading = $('#loading');
  let loadingWait = 100;
  if ($('.blog-page-wrapper').length) {
    loadingWait = 1000;
  }
  setTimeout(() => $loading.fadeOut(500, () => {
    setTimeout(function () {
      WayPoints();
      $('.woocommerce-account').css('overflow', 'auto');
    }, 50);
  }), loadingWait);
});

