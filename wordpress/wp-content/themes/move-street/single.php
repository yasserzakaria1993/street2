<?php
get_header();
?>

<div class="apply-page-wrapper">
  <section class="main-section">
    <img src="<?php echo the_field('main_image'); ?>" alt="" class="bg">
    <div class="container">
      <div class="row justify-content-center align-items-center">
        <div class="col-12 col-lg-8">
          <h2 class="mos-title-1 iv-wp-from-left" style="text-align:center"><?php the_title(); ?></h2>

        </div>

      </div>
    </div>
  </section>
  <section class="why-work-with-us-section" style="margin-top:-100px">
    <div class="container">
        <div class="filter"></div>
      <div class="row justify-content-center">
        <div class="col-12">
        <h3 class="title iv-wp-from-left" style="visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><?php the_title(); ?></h3>
        
        <h2 class="subtitle iv-wp-from-right"><?php the_field('content',false); ?></h2>
        </div>
        <div class="aspect-ratio">
          <svg preserveAspectRatio="none" viewBox="0 0 1 1">
            <defs>
              <clipPath clipPathUnits="objectBoundingBox" id="hero-clip-path">
                <path d="M0 0 H1 V1 L.14 .528 L0 .73 Z"/>
              </clipPath>
            </defs>
            <path d="M0 0H1V.834L.862 1L0 .6Z" fill="#f6fafe"/>
          </svg>
        </div>
      </div>
    </div>
  </section>



</div>
<?php
get_footer();
