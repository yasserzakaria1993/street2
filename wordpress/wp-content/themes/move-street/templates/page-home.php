<?php
/**
 * The main template file
 *
 * Template Name: HomePage
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Move_Street
 */

get_header();
?>
<?php if (have_rows('layout')) {
	
	while (have_rows('layout')) {
		the_row();
		?>
		
		<?php if (get_row_layout() == 'hero_section'):
			?>
      <div class="home-page-wrapper">
      <section class="main-section" style="background-image: url(<?php echo the_sub_field('background_image'); ?>);">
        <div class="main-section-content container">
          <div class="row">
            <div class="col-12">
              <h2 class="mos-title-1 text-center iv-wp-from-bottom"><?php the_sub_field('big_title'); ?></h2>
            </div>
            <div class="col-12">
              <h3 class="mos-title-3 text-center iv-wp-from-bottom"><?php the_sub_field('small_slogan'); ?></h3>
            </div>
          </div>
          <!-- Estimate a cost -->
          <div class="row">
            <div class="col-12 d-none d-lg-block">
              <div class="main-section-form iv-wp-from-bottom">
								<?php
								$form_id = '2';
								gravity_form($form_id, false, false, false, '', true, 12);
								?>
              </div>
            </div>
            
            <div class="col-12 d-block d-lg-none">
              <button class="main-btn make-booking-btn iv-wp-from-bottom">
                Make a Booking Today
              </button>
            </div>
          </div>
					<?php if (have_rows('rating')){ ?>
					<?php while (have_rows('rating')){
					the_row(); ?>
          <div class="facebook-reviews-widgets iv-wp-from-bottom">
            <div class="fb-review-child top-one">
              <img alt="alt text" src="<?php echo get_template_directory_uri(); ?>/assets/images/home/ratings.png">
              <p>Rated 5.0/5.0 based on 45,116 reviews</p>
            </div>
            <div class="fb-review-child middle-one">
              <img alt="alt text" src="<?php echo get_template_directory_uri(); ?>/assets/images/home/facebook-like.png">
              <p>You and 4.5k people like this</p>
            </div>
            <div class="fb-review-child ">
              <img alt="alt text" src="<?php echo get_template_directory_uri(); ?>/assets/images/home/img_01.png">
              <img alt="alt text" src="<?php echo get_template_directory_uri(); ?>/assets/images/home/img_02.png">
            </div>
          </div>
<!--          <div class="facebook-reviews-widgets iv-wp-from-bottom">-->
<!--            <div class="fb-review-child top-one">-->
<!--              <img alt="alt text" src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/home/ratings.png">-->
<!--              <p>Rated --><?php //the_sub_field('rated_review_number'); ?><!--/5.0 based on --><?php //the_sub_field('total_reviews_number'); ?><!-- reviews</p>-->
<!--            </div>-->
						<?php }
						} ?>
<!--            <div class="fb-review-child middle-one iv-wp-from-bottom">-->
<!--              <div data-colorscheme="dark" class="fb-like iv-wp-from-bottom" data-href="https://www.facebook.com/GothamByNight/" data-width="" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>-->
<!--            </div>-->
<!--          </div>-->
        </div>
      </section>
		
		<?php elseif (get_row_layout() == 'why_choose_us_section'):
			?>
      <section class="three-easy-steps-section">
        <div class="section-container container">
          <div class="row d-lg-none d-flex">
            <div class="col-12 col-lg-5">
              <div class="service-section-form">
                <div class="input-group-wrapper">
                  <div class="input-wrapper">
                    <input placeholder="Pick-up Postcode" type="text"/>
                  </div>
                  <div class="input-wrapper">
                    <select>
                      <option value="1">Pick-up Property Type</option>
                      <option value="1">asdas</option>
                      <option value="1">asdas</option>
                      <option value="1">asdas</option>
                    </select>
                  </div>
                  <div class="input-wrapper">
                    <select>
                      <option value="1">Property Size</option>
                      <option value="1">asdas</option>
                      <option value="1">asdas</option>
                      <option value="1">asdas</option>
                    </select>
                  </div>
                  <div class="input-wrapper">
                    <input placeholder="Drop-off Postcode" type="text"/>
                  </div>
                  <div class="input-wrapper">
                    <input placeholder="Email" type="text"/>
                  </div>
                  <div class="input-wrapper">
                    <input placeholder="Mobile (optional)" type="text"/>
                  </div>
                </div>
                <button class="view-price-btn main-btn" type="submit">VIEW PRICE</button>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <h3 class="mos-title-4 iv-wp-from-bottom">WHY CHOOSE US</h3>
            </div>
            <div class="col-12">
              <h2 class="mos-title-2 iv-wp-from-bottom"><?php the_sub_field('steps_big_title'); ?></h2>
            </div>
          </div>
					<?php if (have_rows('steps')) { ?>
						<?php while (have_rows('steps')) {
							the_row();
							?>
              <div class="three-steps-container row">
                <div class="col-12 col-md-12 col-lg-4">
                  <div class="step-content">
                    <img class="iv-wp-from-top" alt="" src="<?php the_sub_field('first_step_image'); ?>">
                    <div class="the-p iv-wp-from-bottom"><?php the_sub_field('first_step_text'); ?></div>
                  </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4">
                  <div class="step-content">
                    <img class="iv-wp-from-top" alt="" src="<?php the_sub_field('second_step_image'); ?>">
                    <div class="the-p iv-wp-from-bottom"><?php the_sub_field('second_step_text'); ?></div>
                  </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4">
                  <div class="step-content">
                    <img class="iv-wp-from-top" alt="" src="<?php the_sub_field('third_step_image'); ?>">
                    <div class="the-p iv-wp-from-bottom"><?php the_sub_field('third_step_text'); ?></div>
                  </div>
                </div>
              </div>
						<?php }
					} ?>
        </div>
      </section>
		<?php elseif (get_row_layout() == 'get_estimation_section'): ?>
      <section class="get-an-estimate">
        <div class="section-container container">
          <div class="row align-items-center justify-content-lg-between justify-content-center">
            <div class="left-col col-12 col-lg-6">
              <img class="iv-wp-from-bottom" alt="" src="<?php the_sub_field('section_main_image'); ?>">
            </div>
            <div class="right-col col-with-number col-11 col-sm-10 col-lg-5 iv-wp-from-bottom">
              <h2 class="mos-title-2" data-mos-before-number="1">
								<?php the_sub_field('section_title'); ?>
              </h2>
              <div class="mos-paragraph">
								<?php the_sub_field('section_description'); ?>
              </div>
							<?php $button = get_sub_field('button'); ?>
              <a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
                <button class="get-estimate-btn sub-btn"><?php echo $button['title']; ?></button>
              </a>
            </div>
          </div>
        </div>
      </section>
		<?php elseif (get_row_layout() == 'meet_your_streetmovers_section'): ?>
      <section class="meet-street-movers">
        <svg class="meet-street-movers-svg" preserveAspectRatio="none" viewBox="0 0 1 1">
          <defs>
            <linearGradient gradientUnits="objectBoundingBox" id="grad1" x1="0.5" x2="0.5" y1="0" y2="1">
              <stop offset="0" style="stop-color:rgb(246,250,254);stop-opacity:1"/>
              <stop offset="1" style="stop-color:rgb(255,255,255);stop-opacity:1"/>
            </linearGradient>
          </defs>
          <path d="M0 0.111732 L 0.140625 0 L 1 0.259777 V1 H0 Z" fill="url(#grad1)"/>
        </svg>
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="left-col col-with-number col-11 col-sm-10 col-lg-7 iv-wp-from-bottom">
              <h2 class="mos-title-2" data-mos-before-number="2">
								<?php the_sub_field('section_title'); ?>
              </h2>
              <div class="mos-paragraph">
								<?php the_sub_field('section_description'); ?>
              </div>
            </div>
            <div class="right-col p-lg-0 col-11 col-lg-5 iv-wp-from-bottom">
              <img alt="" src="<?php the_sub_field('main_image'); ?>">
            </div>
          </div>
        </div>
      </section>
		<?php elseif (get_row_layout() == 'relax'): ?>
      <section class="relax-section">
        <div class="container">
          <div class="row justify-content-center">
            <div class=" col-11 col-sm-10 col-with-number iv-wp-from-bottom">
              <h2 class="mos-title-2" data-mos-before-number="3">
								<?php the_sub_field('section_title'); ?>
              </h2>
              <div class="mos-paragraph">
								<?php the_sub_field('section_description'); ?>
              </div>
							<?php $button = get_sub_field('button'); ?>
              <a href="<?php echo $button['url']; ?>" target="<?php echo $button['target'] ?>">
                <button class="our-moving-service sub-btn"><?php echo $button['title']; ?></button>
              </a>
            </div>
          </div>
        </div>
        <div class="bottom-img">
          <img alt="" src="<?php the_sub_field('main_image_below_the_section'); ?>">
        </div>
      </section>
		<?php elseif (get_row_layout() == 'finish'): ?>
      <section class="finish-in-one-step">
        <div class="section-container container">
          <div class="row align-items-end justify-content-lg-between justify-content-center">
            <div class="left-col col-11 col-lg-6 iv-wp-from-bottom">
              <img alt="" src="<?php the_sub_field('main_image'); ?>">
            </div>
            <div class="right-col col-with-number col-11 col-sm-10 col-lg-5 iv-wp-from-bottom">
              <h2 class="mos-title-2" data-mos-before-number="4">
								<?php the_sub_field('section_title'); ?>
              </h2>
              <div class="mos-paragraph">
								<?php the_sub_field('section_description'); ?>
              </div>
            </div>
          </div>
          <div class="col-11 col-sm-10 mx-auto text-center finish-in-one-step-btn iv-wp-from-bottom">
						<?php $button = get_sub_field('button'); ?>
            <a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
              <button class="sub-btn-2"><?php echo $button['title']; ?></button>
            </a>
          </div>
        </div>
      </section>
		<?php elseif (get_row_layout() == 'compare_to_others'): ?>
      
      <section class="compare-table-section">
        <div class="section-container container">
          <h2 class="mos-title-2 iv-wp-from-bottom">
						<?php the_sub_field('section_title'); ?>
          </h2>
					<?php // TODO: Table backend doesn't exist yet, ASK Yasser ?>
          <table class="compare-table iv-wp-from-bottom">
            <tr>
              <th></th>
              <th><img src="<?php echo get_template_directory_uri(); ?>/assets/images/home/logo-table.png" alt=""></th>
              <th><img src="<?php echo get_template_directory_uri(); ?>/assets/images/home/logo2.png" alt=""></th>
              <th><img src="<?php echo get_template_directory_uri(); ?>/assets/images/home/logo3.png" alt=""></th>
            </tr>
            <tr>
              <td>First pickup is completely free</td>
              <td><i class="fal fa-check"></i></td>
              <td><i class="fal fa-times"></i></td>
              <td><i class="fal fa-times"></i></td>
            </tr>
            <tr>
              <td>Free packing bins, save on boxes and tape</td>
              <td><i class="fal fa-check"></i></td>
              <td><i class="fal fa-times"></i></td>
              <td><i class="fal fa-times"></i></td>
            </tr>
            <tr>
              <td>Online photo inventory of what’s in storage</td>
              <td><i class="fal fa-check"></i></td>
              <td><i class="fal fa-times"></i></td>
              <td><i class="fal fa-times"></i></td>
            </tr>
            <tr>
              <td>Free Goodwill donation pickups</td>
              <td><i class="fal fa-check"></i></td>
              <td><i class="fal fa-times"></i></td>
              <td><i class="fal fa-times"></i></td>
            </tr>
          </table>
        
        </div>
      </section>
		
		<?php elseif (get_row_layout() == 'customer_reviews'): ?>
			<?php // TODO: I need to create post type called reviews to make reviews working with a loop here! ?>
      <section class="testimonial-section" style="background:url(<?php the_sub_field('background_image'); ?>)">
        <div class="section-container container">
          <h2 class="mos-title-2 iv-wp-from-bottom">
						<?php the_sub_field('section_title'); ?>
          </h2>
          
          <!-- This is customer reviews loop -->
					<?php
					// WP_Query arguments
					$args = array('post_type'      => array('customer_reviews'),
					              'posts_per_page' => 3);
					
					// The Query
					$customer_reviews = new WP_Query($args);
					
					// The Loop
					if ($customer_reviews->have_posts()) {
						?>
            <div class="testimonial-slider iv-wp-from-bottom">
							<?php
							while ($customer_reviews->have_posts()) {
								$customer_reviews->the_post();
								?>
                <div class="testimonial-slider-item">
                  <div class="desc">
										<?php the_field('client_review'); ?>
                    <img class="the-img" src="<?php the_field('client_image'); ?>" alt="">
                  </div>
                  <div class="stars">
										<?php $review_number = get_field('client_rating'); ?>
										<?php RatingSystem($review_number); ?>
                  </div>
                  <div class="name"><strong>Brookelynn P </strong>- Mandurah, Perth</div>
                </div>
								<?php
							}
							?>
            </div>
            <div class="text-center more-customer-reviews iv-wp-from-bottom">
              <button class="sub-btn">More Customer Reviews</button>
            </div>
						<?php
					} else {
						echo "<h2 class='not-found'>No Reviews added yet</h2>";
					}
					// Restore original Post Data
					wp_reset_postdata();
					
					?>
        </div>
      </section>
		<?php elseif (get_row_layout() == 'client_logos'): ?>
      <section class="clients-section">
        <div class="section-container container">
          <div class="clients-slider iv-wp-from-bottom">
						<?php if (have_rows('logos')){
						while (have_rows('logos')) {
							the_row();
							$logo = get_sub_field('logo');
							?>
              <div class="testimonial-slider-item">
                <img src="<?php echo $logo; ?>" alt="">
              </div>
						
						<?php } ?>
          </div>
					<?php } ?>
        </div>
      </section>
		<?php endif;
	}
} ?>
  <!-- <script type="text/javascript">
	
	jQuery('#u_0_5').css("display","none");
	
	</script> -->
<?php
get_footer();
