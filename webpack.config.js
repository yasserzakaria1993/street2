const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: "./src/index.js",
    // mode: 'production',
    mode: "development",
    output: {
      // filename: 'bundle.js',
         filename: '../wordpress/wp-content/themes/move-street/assets/js/bundle.js',
        path: path.resolve(__dirname, "./public/"),
        publicPath: "/",
    },
    watch: true,
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/env"],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                            "@babel/plugin-proposal-object-rest-spread",
                            "@babel/plugin-proposal-export-default-from",
                            "@babel/plugin-proposal-export-namespace-from",
                        ],
                    },
                },
            }, {
                test: /\.s?css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    // {loader: "style-loader"},
                    {loader: "css-loader", options: {importLoaders: 1, sourceMap: true, }},
                    {loader: "postcss-loader", options: {}},
                    {loader: "sass-loader", options: {sourceMap: true}},
                ],
            }, {
                test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        publicPath: function (url) {
                            return "./" + url;
                        },
                        name: "fonts/[name].[ext]",
                        // outputPath: 'fonts',
                    },
                }],
            }, {
                test: /node_module.*\.(png|svg|jpg|gif)$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        publicPath: function (url) {
                            return "./" + url;
                        },
                        name: "assets/[folder]/[name].[ext]",
                    },
                }],
            }, {
                test: /\.(png|svg|jpg|gif)$/,
                exclude: /node_modules/,
                use: [{
                    loader: "file-loader",
                    options: {
                        publicPath: function (url) {
                            // console.error(url);
                            return "./" + url;
                        },
                        name: "[path][name].[ext]",
                        context: path.resolve(__dirname, "./src/html"),
                    },
                }],
            },
        ],
    },
    optimization: {
        sideEffects: false,
        minimizer: [new OptimizeCSSAssetsPlugin({})],
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: "[file].map",
            exclude: ["/vendor/"],
        }),
        new MiniCssExtractPlugin({
            // // Options similar to the same options in webpackOptions.output
            // // both options are optional
             filename: '../wordpress/wp-content/themes/move-street/assets/css/bundle.css',
            // chunkFilename: "[id].css"
        }),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: path.resolve(__dirname, "./src/html/home.html"),
        }),
        new HtmlWebpackPlugin({
            filename: "apply.html",
            template: path.resolve(__dirname, "./src/html/apply.html"),
        }),
        new HtmlWebpackPlugin({
            filename: "service.html",
            template: path.resolve(__dirname, "./src/html/service.html"),
        }),
        new HtmlWebpackPlugin({
            filename: "blog.html",
            template: path.resolve(__dirname, "./src/html/blog.html"),
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            select2: "select2",
            slick: "slick-carousel",
            isotope: "isotope-layout",
            imagesLoaded: "imagesloaded",
            jqueryBridge: "jquery-bridget",
            zoom: "jquery-zoom",
            // Swupjs: 'swupjs',
        }),
    ],
    // devtool: 'source-map',
    devtool: "cheap-module-eval-source-map",
    devServer: {
        contentBase: path.resolve(__dirname, "./public"),
        historyApiFallback: true,
        
    },
};
